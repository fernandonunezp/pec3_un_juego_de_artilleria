using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bola_de_fuego : MonoBehaviour {

	//Volumen de colisión
	public Rigidbody2D rb;
	//Velocidad del proyectil
	public Vector2 velocidad;


	void Start(){
		//El objeto tiene 10 segundos de vida
		Destroy (this.gameObject, 10);
		//Se le asigna un rigidbody que colisionará con el entorno
		rb = GetComponent<Rigidbody2D>();
		velocidad = rb.velocity;

	}

	void FixedUpdate(){
		if(rb.velocity.y > velocidad.y)
			rb.velocity = velocidad;
	}

	//Al chocar
	void OnCollisionEnter2D(Collision2D col){
		//Va en nuestra dirección pero cayendo
		rb.velocity = new Vector2(velocidad.x, -velocidad.y);

		//Si choca con un enemigo
		if (col.collider.tag == "Enemy"){
			Destroy(col.gameObject);
			explotar();
		}

		//Si choca con algo (0 indica un mínimo de tolerancia al choque)
		if(col.contacts[0].normal.x != 0 )
			explotar();
	}

	void explotar(){
		//Reproducir sonido
		GetComponent<AudioSource>().Play();
		//Eliminar
		Destroy(this.gameObject);
	}
}
