using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class camara_juego : MonoBehaviour {

	//En qué se fija la cámara
	public Transform objetivo;
	//Límites que la cámara no puede pasar
	public Transform limiteIzq;
	public Transform limiteDrc;

	//Variables de suavizado de movimiento
	private float smoothDampTime = 0.15f;
	private Vector3 smoothDampVelocity = Vector3.zero;

	//Valores que nos dan el alto y ancho de la cámara, y el x minimo y máximo para que la cámara comience el seguimiento(o lo detenga).
	private float anchCam, altCam, movMin, movMax;
/*
	//Agitación de cámara como efecto dramático
	IEnumerator Camaragitar(){				//Lo llamamos como coroutine para que no interrumpa la explosion
		//Variable de tiempo
		float lapso = 0.0f;
		//Solo 1 segundo
		while (elapsed < 0.7f){
			//Posiciones aleatorias momentaneas
			float x = Random.Range(-1f, 1f) * 0.4f;
			float y = Random.Range(-1f, 1f) * 0.4f;
			//Se actualiza
			transform.localPosition = new Vector3(x, y, posactual.z);
			//Se aumenta el tiempo
			elapsed += Time.deltaTime;
			yield return null;
		}
	}
*/
	// Los valores aqui establecidos se inicializan en base a cualquier tipo de cámara, se puede aplicar casi cualquier plataformas 2D.
	// Hay que tener en cuenta que en el Super Mario Bros original no se variaba el seguimiento en función de la altura, por lo que
	// se obvia tenerlos en cuenta.
	void Start () {
		//Altura y ancho de cámara sacado de sus propiedades en el editor
		altCam = Camera.main.orthographicSize * 2;
		anchCam = altCam * Camera.main.aspect;

		//Valores que se cogen de los límites del propio juego ya que tambien se deben tener en cuenta por el hecho de que no se vean
		float anchLimIzq = limiteIzq.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;
		float anchLimDrc = limiteDrc.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;
	
		//Límites para comenzar el seguimiento al ir a la derecha o a la izquierda
		movMin= limiteIzq.position.x + anchLimIzq + (anchCam/2);
		movMax= limiteDrc.position.x - anchLimDrc - (anchCam/2);
	}
	
	// Aqui se comprueba en cada frame la posición del objetivo y si esta condiciona el cambio de posición de la cámara pues ésta se actualiza
	void Update () {
		
		if (objetivo){
			//Lo que nos da esta función es la posición del objetivo en relacion a los bordes, obteniendo un valor que nos indica donde debe estar la cámara posicionada
			//no solo en relacion al personaje si no tambien en relacion a los bordes
			float posObjetivoX = Mathf.Max (movMin, Mathf.Min(movMax, objetivo.position.x));
			//float posObjetivoY = Mathf.Max (movMin, Mathf.Min(movMax, objetivo.position.y));
		
			//Esta variable se utiliza para suavizar el movimiento y que la cámara no sea tan estática
			float posX = Mathf.SmoothDamp(transform.position.x, posObjetivoX, ref smoothDampVelocity.x, smoothDampTime);
			//float posY = Mathf.SmoothDamp(transform.position.y, posObjetivoY, ref smoothDampVelocity.y, smoothDampTime);

			// **Empleo un Vector 3 debido a un inconveniente con Unity
			transform.position = new Vector3(posX, transform.position.y, transform.position.z);
		}
	}
}
