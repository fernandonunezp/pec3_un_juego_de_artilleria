Introducción
Proyecto en Unity réplica del primer nivel del juego Super Mario Bros con funcio
-nalidades adaptadas.

MODO DE JUEGO
Comienza el nivel y nos podemos desplazar hacia la izquierda o hacia la derecha 
con las teclas de dirección, además de poder saltar con la barra espaciadora.

Acabado el nivel(sea por muerte o por victoria) se nos mostrará una pantalla 
desde la cual podemos rejugar el nivel o salir del juego, las opciones se esco
-gen con las teclas de dirección y se confirma la elección con la barra 
espaciadora.

ESTRUCTURA DEL PROYECTO

		Assets
		  |
		  |-Animaciones
		  | |
		  | |-Bloques
		  | |
		  | |-Goomba
		  | |
		  | |-Mario
		  |
		  |-Musica
		  | |
		  | |-FX
		  |
		  |
		  |-Resources	
		  |	|
		  | |-Prefabs
		  |  
		  |-Scenes
		  |	
		  |-Scripts
		  |	
		  |-Sprites	
		 	
Resources: contiene ls carpeta Prefabs en la que están organizados todos los 
elementos necesarios para la creación del juego mediante click and drag. De este
modo la creación de nuevos niveles se hace mucho mas cómodamente.

Scenes: contiene las escenas que componen el juego,en este caso fase(nuestro 
nivel) y fin(la pantalla de fin del juego).

Scripts: contiene los scripts que emplean los GameObjects y las clases de datos
usadas en el proyecto.

Sprites: contiene los Sprites que aparecen en el juego, o componentes de esos
sprites.

Animaciones: contiene las animaciones que emplearan los elementos del juego, se
-paradas en las animaciones de Mario, las de Goomba, y las de los bloques int, 
ya que las animaciones de los bloques normales no se añadieron.

Musica: Música y efectos de sonido que se emplean en el juego.


EXPLICACION DE CLASES, SCRIPTS Y DISEÑO

En este juego la base principal de gameplay reside en las colisiones entre 
objetos y las reacciones que estas causan. Para detectar estas colisiones he 
decidido emplear RayCasting; la proyección de rayos invisibles en puntos 
estratégicos cercanos a la hitbox del elemento propio que detectan otras
hitboxes.

En el caso de cualquier elemento móvil del juego, necesitamos detectar muros y
suelos, por lo que las funciones de Raycasting de muro y suelo las poseen 
tanto enemigos, como setas(no implementadas), como Mario.

Para el cálculo de colisiones, se emplea la altura del personaje(calculada en la
propia funcion de Raycasting con una auxiliar llamada "poder") y con pequeños
ajustes a la izquierda y a la derecha para obtener puntos en todo el perímetro 
del elemento.

-comprRaycastingMuros(Vector2 pos, float direccion): Esta funcion proyecta rayos 
al lado correspondiente a la dirección del elemento(indicada por su valor 
Scale.x) y detecta colisiones con todas aquellas capas que definamos como
"muros" en el Inspector. 
Si se detecta un muro, dependiendo de quien lo toque:
    -En el caso de Mario los muros serían la capa Default, en la que se encuen-
    tran tanto elementos sólidos del escenario (tubos, bloques,el propio suelo) 
    como los enemigos y la meta, ya que debemos detectar la colisión con esta
    para completar el nivel. Al tocarlos La posición de Mse recalcula como la que 
    tenía en el momento de la colisión, permitiendole moverse hacia el tubo ya 
    que esto le aporta kinestesia al juego, dano la senasación de que Mario se 
    sigue moviendo a pesar de que su posición se mantenga en un punto fijo en 
    base al objeto colisionado.

    -Para los enemigos las capas de colision en muros son la Default y el propio
    Mario. Al tocarlas, el enemigo cambia su scale.x a negativo, lo que hace que
    en su función de movimiento invierta su dirección.

    -En caso de estas implementada la seta la reacción sería igual a la de los
    enemigos.

-comprRaycastingSuelo(Vector2 pos): Igual que la funcion de arriba pero con rayos
proyectados a la base de la figura, detectando colisiones con aquellas capas que
definamos como "suelo" en el Inspector. Para Mario, tambien contarían como suelo
los enemigos. Al colisionar con un suelo, la posición del elemento móvil que 
colisiona se mantiene en su eje "y" como por encima del objeto con el que ha 
colisionado(en base a la altura propia del elemento movil), de este modo nuestro
elemento móvil recorre la superficie de los elementos del escenario en vez de 
atravesarlos.

En el caso de Mario, también se deben tener en cuenta las colisiones con el 
techo, por lo que tambien tenemos la funcion:

-comprRaycastingTecho(Vector2 pos): Otra funcion de Raycasting que realiza una 
función similar a la de suelos, solo que al detectar una colision con aquella
capa que definamos como "techo", pone a Mario en estado de caída, devolviendolo 
rápidamente al suelo.

CLASES
Las clases que emplean los elementos de esta práctica son:

*elemento_movil: Esta clase define las características comunes a todo elemento que
se mueva en el escenario(sea Mario, una seta, un enemigo, etc).

Atributos
	public float gravedad: Valor que indica la magnitud de la gravedad que afec
	-ta al elemento. Es pública ya que la ajustamos desde el inspector.
	protected bool tocaSuelo: Booleano que indica si el elemento está en contacto
	con el suelo.
	protected float estatura: Estatura del elemento, empleada en el calculo de 
	colisiones.
	protected int poder: Variable de control de altura para detección de colisiones
	protected enum estado_index: Define los multiples estados en los que se 
	puede encontrar el elemento.
	protected estado_index estado: Estado actual del elemento empleado para 
	determinar animaciones, fisicas que afectan, etc.
	public Vector2 velocidad: Velocidad de movimiento del elemento, también la 
	definimos a través del inspector.
	public LayerMask muros: Referencias de muros con los que colisionar.
	public LayerMask suelo: Referencia a la capa del suelo que tendrán en cuentan
	los RayCast.

Funciones
	protected void caer(): Función que manda al elemento al suelo, poniendolo 
	en estado de caída.
	protected abstract void caerAlVacio(): Que pasa al caer al vacio
	protected abstract void movimiento(): Como se mueve el elemento
	protected abstract IEnumerator morir():	Que proceso sigue al morir/destruirse.
	protected abstract Vector2 comprRaycastingSuelo(Vector2 pos): Comprobacion 
	de colisiones con elementos que toquemos con la parte baja del collider.
	protected abstract Vector2 comprRaycastingMuros(Vector2 pos, float direccion): 	
	Comprobacion de colisiones con elementos que toquemos horizontalmente

NOTA: Al heredar las dos clases siguientes de ésta, no vuelvo a comentar los
métodos heredados; a pesar de que estas clases redefinen estos métodos.

*elemento_vivo: Movimiento e interacción básica de un enemigo con el escenario y
sus elementos.

Funciones
    public void aplastado(): Función que se llama cuando Mario pisa al elemento.
    Es pública ya que la invoca el colisionador de suelo de Mario.
    private void OnBecameVisible(): Función interna de Unity que hace que el 
    elemento se active al volverse visible por una cámara. Esto es muy útil ya 
    que de no emplearla, los enemigos que encontrásemos más adelante igual no 
    aparecerían ya que se habrían caído por el vacío.

*char_mario: Clase que define a Mario y sus funciones.

Atributos
	private int puntuacion: La puntuación que obtenemos a lo largo del nivel, no
	la total en todo el juego.
	private bool finalizado: Para controlar si hemos tocado la meta, e iniciar 
	la secuencia adecuada.
	private bool mover_izq, mover_drc, saltar, mover: Booleanos que le indican
	a la función de movimiento qué acción/es tomar. 
	public float saltoVel: Potencia de salto de Mario.
	public Camera camara: Camara que observa a Mario. Se utiliza para reproducir
	determinados sonidos dados ciertos eventos.
	public AudioClip[] audioArray: Array que contiene sonidos que puede producir
	Mario.

Funciones
	void adquirir_powerup(): Se llama al tocar un power-up.
	void crecer():Se llama cuando Mario pasa de estado pequeño a grande.
	void encoger(): Se llama cuando Mario pasa de estado grande a pequeño.
	public void recibirGolpe():	Se llama cuando se toca a un enemigo/elemento 
	dañino no mortal del escenario. Es pública ya que la llama el enemigo.
	void dannyBoi(): Realiza las preparaciones previas a la muerte y activa la
	secuencia de derrota.
	protected override IEnumerator morir(): Inicia la secuencia de derrota.
	//Iniciar la secuencia de victoria
	void iniVictor(): Realiza las preparaciones previas a la victoria y activa la
	secuencia de victoria.
	IEnumerator victoria(): Realiza la secuencia de victoria.
	void pulsarBoton(): Comprueba qué botones están siendo pulsados y activa las 
	variables consecuentes.
	void cambioAnimacion(): Cambia las animaciones cuando tienen lugar 
	determinadas condiciones.


*camara_juego: Clase que define la funcionalidad de la cámara. Realiza el seguimi
ento de un objetivo de forma horizontal acompañando su movimiento a lo largo del
juego. Como añadido en el seguimiento emplea una función de suavizado de movi-
miento para no dañar la vista del jugador al desplazarse.

Atributos
	public Transform objetivo: En qué se fija la cámara.
	public Transform limiteIzq: Límites que la cámara no puede pasar, ya que de 
	hacerlo se verían partes del escenario no editadas.
	public Transform limiteDrc: Igual que el anterior.
	private float smoothDampTime: Variable que indica el tiempo de suavizado has
	-ta que la cámara se vuelve a centrar en el objetivo.
	private Vector3 smoothDampVelocity: Vector empleado en el suavizado de 
	movimiento.
	private float anchCam, altCam, movMin, movMax: Valores que nos dan el alto y
	ancho de la cámara, y el x minimo y máximo para que la cámara comience el 
	seguimiento(o lo detenga)

*menu_final: Funcionalidad de la pantalla de fin del juego. LA clase solo hace
aparecer y desaparecer un puntero al lado de cada opción, permitiéndole visu-
almente al jugador escoger una opción cuando pulsa "Espacio".

Atributos
	public RectTransform punteroA: Imagen que representa el puntero de selcción
	nº1.
	public RectTransform punteroB: Imagen que representa el puntero de selcción
	nº1.
	bool val: Booleano usado para la elección de opción.



ERRORES DE LA PRACTICA

-La música de game over no se reproduce correctamente, en su lugar tan solo se 
detiene la música principal del juego.

-Al morir Mario tocado por un goomba, si le vuelve a tocar otro goomba reprodu-
ce el sonido de encogimiento, a pesar de estar ya muerto.

-Los goombas no chocan entre si, se atraviesan en su lugar.

-Es posible que si a Mario le cae encima goomba no resulte dañado.


